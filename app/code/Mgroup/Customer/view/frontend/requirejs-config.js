/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            passwordStrengthIndicatorCus: 'Mgroup_Customer/js/password-strength-indicator'
        }
    }
};
