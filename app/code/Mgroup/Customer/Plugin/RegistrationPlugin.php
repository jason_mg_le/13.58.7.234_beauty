<?php
namespace Mgroup\Customer\Plugin;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Customer\Model\Registration;

class RegistrationPlugin
{
    /**
     * @param Registration $subject
     * @param boolean $result
     */
    public function afterIsAllowed(Registration $subject, $result)
    {
        return true;
    }
}