<?php
namespace Mgroup\Customer\Observer;


use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Registry;

class Register implements ObserverInterface
{
    protected $coreRegistry;

    public function __construct(Registry $registry)
    {
        $this->coreRegistry = $registry;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customer = $observer->getEvent()->getCustomer();
        $customerEmail = $customer->getEmail();
        //if (!empty($customerEmail)) {
            $this->coreRegistry->register('new_account_email', true);
        //}
        
    }
}