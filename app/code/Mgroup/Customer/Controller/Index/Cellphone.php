<?php
namespace Mgroup\Customer\Controller\Index;
use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Api\AccountManagementInterface;

class Cellphone extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $customer;
    /** @var AccountManagementInterface */
    protected $customerAccountManagement;

    public function __construct(
        \Magento\Backend\App\Action\Context $context
        ,\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        AccountManagementInterface $customerAccountManagement,
        \Magento\Customer\Model\Customer $customer
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->customer = $customer;
    }

    /**
     * View blog homepage action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $cellphone = $this->getRequest()->getParam('signup_cell');
        $rs = $this->checkCellPhoneExists($cellphone);
        return $result->setData($rs);
    }

    protected function checkCellPhoneExists($cellphone, $websiteId = null)
    {
        $customer = $this->customer;
        $customer->setWebsiteId(1);
        if ($websiteId) {
            $customer->setWebsiteId($websiteId);
        }
        $obj = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Customer\Model\ResourceModel\Customer\Collection::class
            );
        $collections = $obj->addAttributeToSelect('*')
                  ->addAttributeToFilter('signup_cell', (string)$cellphone)
                  ->load();
        $count = $collections->getSize();
        if ((int) 1=== (int)$count) {
            return '此手機號已被註冊過';
        }
        return true;
    }

}
