<?php
namespace Mgroup\Customer\Controller\Index;
use Magento\Framework\Controller\ResultFactory;
/**
 * Blog home page view
 */
class Index extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $customer;
    public function __construct(
        \Magento\Backend\App\Action\Context $context
        ,\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\Customer $customer
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->customer = $customer;
    }

    /**
     * View blog homepage action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $email = $this->getRequest()->getParam('email');
        $rs = $this->checkEmailExists($email);
        return $result->setData($rs);
    }

    protected function checkEmailExists($email, $websiteId = null)
    {
        $customer = $this->customer;
        $customer->setWebsiteId(1);
        if ($websiteId) {
            $customer->setWebsiteId($websiteId);
        }
        $customer->loadByEmail($email);
        if ($customer->getId()) {
            return '此 '.$email.' 已被註冊過';//$customer->getId();
        }
        return true;
    }

}
