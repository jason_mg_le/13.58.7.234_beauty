<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Mgroup\Customer\Controller\Plugin;

use Magento\Customer\Model\Session;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;

class Account
{
    /**
     * @var Session
     */
    protected $session;
    /** @var AccountManagementInterface */
    protected $customerAccountManagement;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        Session $customerSession,
        AccountManagementInterface $customerAccountManagement
    ) {
        $this->session = $customerSession;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->_request = $context->getRequest();
        $this->_response = $context->getResponse();
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
        $this->resultFactory = $context->getResultFactory();
    }
    public function aroundExecute(\Magento\Customer\Controller\Account\LoginPost $subject, $proceed)
    {           
        $login =  $this->_request->getPost('login');
        $custom_redirect= false;

        $returnValue = $proceed();
        $isLoggedIn = $this->session->isLoggedIn();
        if (!$isLoggedIn) {
            $userName = $login['username'];
            $obj = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Customer\Model\ResourceModel\Customer\Collection::class
            );
            $collections = $obj->addAttributeToSelect('*')
                  ->addAttributeToFilter('user_name', $userName)
                  ->load();
            $count = $collections->getSize();
            if ( 1 === (int)$count) {
                $email = $collections->getFirstItem()->getEmail();
                $customer = $this->customerAccountManagement->authenticate($email, $login['password']);
                $this->session->setCustomerDataAsLoggedIn($customer);
                $this->session->regenerateId();
                
            } else {
                $obj1 = \Magento\Framework\App\ObjectManager::getInstance()->create(
                    \Magento\Customer\Model\ResourceModel\Customer\Collection::class
                );
                $collections1 = $obj1->addAttributeToSelect('*')
                  ->addAttributeToFilter('signup_cell', (string)$userName)
                  ->load();
                $count = $collections1->getSize();
                if ((int)$count === (int)1) {
                    $email = $collections->getFirstItem()->getEmail();
                    $customer = $this->customerAccountManagement->authenticate($email, $login['password']);
                    $this->session->setCustomerDataAsLoggedIn($customer);
                    $this->session->regenerateId();
                }
            }
        }
        return $returnValue;
    }
}
