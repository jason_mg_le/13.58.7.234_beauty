<?php
namespace Mgroup\Customer\Controller\Create;
use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\View\Result\PageFactory;
/**
 * Blog home page view
 */
class Success extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;

    protected $customer;
    /**
     * @var Session
     */
    protected $session;
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    protected $customerUrl;
    /**
     * @var \Magento\Framework\Indexer\IndexerInterfaceFactory
     */
    protected $indexerFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        Session $customerSession
        ,\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        CustomerUrl $customerHelperData,
        PageFactory $resultPageFactory,
        \Magento\Framework\Indexer\IndexerInterfaceFactory $indexerFactory,
        \Magento\Customer\Model\Customer $customer
    ) {
        parent::__construct($context);
        $this->session = $customerSession;
        $this->customerUrl = $customerHelperData;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->indexerFactory = $indexerFactory;
        $this->customer = $customer;
    }
    /**
     * View blog homepage action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $email = $customerName = '';
        $isEmail = false;
        if ($this->session->isLoggedIn()) {
            $customer = $this->session->getCustomer();//->getEmail();
            //$customerName = $customer->getFirstname().' '.$customer->getLastname();
            $customerName = $customer->getUserName();
            $email = $customer->getEmail();
            $pattern = '/noemail/';
            if (preg_match($pattern, $email, $matches)){
                $email = 'noemail123@gmail.com';
            }
            if (!empty($email) && 'noemail123@gmail.com' != $email) {
                $isEmail = true;
            } else {
                $isEmail = false;
            }
        } else {
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('customer/account/register');
            return $resultRedirect;
        }
        $resultPage = $this->resultPageFactory->create();
        if ($isEmail) {
            $resultPage->addHandle('custtomer_create_success_email');
            $value = $this->customerUrl->getEmailConfirmationUrl($email);
            $resultPage->getLayout()->getBlock('success.register.email')->setEmail($email)->setEmailConfirm($value);
        } else {
            $resultPage->getLayout()->getBlock('success.register')->setName($customerName);
        }
        $this->executeIndex();
        return $resultPage;
    }

    /**
     * @param string $indexerId
     * @return \Magento\Framework\Indexer\IndexerInterface
     */
    public function getIndexer($indexerId)
    {
        return $this->indexerFactory->create()->load($indexerId);
    }

    public function executeIndex() {

        $this->getIndexer('customer_grid')->reindexAll();
    }
}