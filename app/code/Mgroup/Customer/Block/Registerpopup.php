<?php
namespace Mgroup\Customer\Block;
use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Registry;
class Registerpopup extends  \Magento\Framework\View\Element\Template
{
    protected $httpContext; 
    protected $coreRegistry;
    /**
     * @var BlockRepositoryInterface
     */
    private $blockRepository;
    protected $_storeManagerInterface;

    public function __construct(
        Registry $registry,
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\App\Http\Context $httpContext,
        BlockRepositoryInterface $blockRepository,
        StoreManagerInterface $storeManagerInterface,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        $this->blockRepository = $blockRepository;
        $this->httpContext = $httpContext;
        $this->_storeManagerInterface = $storeManagerInterface;
        parent::__construct(
            $context,
            $data
        );
        $this->_isScopePrivate = true;
    }
    
    public function _prepareLayout()
    { 

        return parent::_prepareLayout();
    }
    
    
    public function getPopupData($identifier = 'register-popup-cn')
    {
        try {
            /** @var BlockInterface $block */
            $currentStore = $this->_storeManagerInterface->getStore();
            $currentStoreCode = $currentStore->getCode();
            $this->coreRegistry->register('custt_current_store', $currentStoreCode);
            $block = $this->blockRepository->getById("register-popup-{$currentStoreCode}");
            $title = $block->getTitle();
            $content = $block->getContent();
        } catch (LocalizedException $e) {
            $content = false;
        }
        $rs = [
            'title' => $title,
            'content' => $content
        ];
        return $rs;
    }
}
