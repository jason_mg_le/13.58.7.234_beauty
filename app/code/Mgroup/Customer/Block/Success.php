<?php
namespace Mgroup\Customer\Block;
use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\Session;
class Success extends  \Magento\Framework\View\Element\Template
{
    protected $httpContext; 
    protected $_logo;
    protected $session;
    /**
     * @var BlockRepositoryInterface
     */
    private $blockRepository;
    protected $_storeManagerInterface;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\App\Http\Context $httpContext,
        BlockRepositoryInterface $blockRepository,
        StoreManagerInterface $storeManagerInterface,
        \Magento\Theme\Block\Html\Header\Logo $logo,
        Session $customerSession,
        array $data = []
    ) {
        $this->blockRepository = $blockRepository;
        $this->httpContext = $httpContext;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_logo = $logo;
        $this->session = $customerSession;
        parent::__construct(
            $context,
            $data
        );
        $this->_isScopePrivate = true;
    }

    public function getCustomerName()
    {
        return $this->getName();
    }

    public function deleteSession()
    {
        try{
            //$randomEmail = 'noemail-'.rand(1,10000).'@test.com';
            //$this->session->getCustomer()->setEmail($randomEmail)->save();
            $this->session->logout();
            $this->session->start();
        }catch(Exception $e){
            throw new Exception($e->getMessage(), 230, null);
        }
    }
}