<?php
namespace Mgroup\Customer\Block;
use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\Session;
class SuccessEmail extends  \Magento\Framework\View\Element\Template
{
    
    protected $httpContext; 
    protected $_logo;
    protected $session;
    /**
     * @var BlockRepositoryInterface
     */
    private $blockRepository;
    protected $_storeManagerInterface;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\App\Http\Context $httpContext,
        BlockRepositoryInterface $blockRepository,
        StoreManagerInterface $storeManagerInterface,
        \Magento\Theme\Block\Html\Header\Logo $logo,
        Session $customerSession,
        array $data = []
    ) {
        $this->blockRepository = $blockRepository;
        $this->httpContext = $httpContext;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_logo = $logo;
        $this->session = $customerSession;
        parent::__construct(
            $context,
            $data
        );
        $this->_isScopePrivate = true;
    }
    public function getLogoSrc()
    {    
        return $this->_logo->getLogoSrc();
    }
    
    /**
     * Get logo text
     *
     * @return string
     */
    public function getLogoAlt()
    {    
        return $this->_logo->getLogoAlt();
    }
    
    /**
     * Get logo width
     *
     * @return int
     */
    public function getLogoWidth()
    {    
        return $this->_logo->getLogoWidth();
    }
    
    /**
     * Get logo height
     *
     * @return int
     */
    public function getLogoHeight()
    {    
        return $this->_logo->getLogoHeight();
    }

    public function getEmailAddress()
    {
        return $this->getEmail();
    }

    public function deleteSession()
    {
        try{
            //$randomEmail = 'noemail-'.rand(1,10000).'@test.com';
            //$this->session->getCustomer()->setEmail($randomEmail)->save();
            $this->session->logout();
            $this->session->start();
        }catch(Exception $e){
            throw new Exception($e->getMessage(), 230, null);
        }
    }
}