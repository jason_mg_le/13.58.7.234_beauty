<?php
namespace Mgroup\Customer\Setup {

    class Recurring implements \Magento\Framework\Setup\InstallSchemaInterface
    {
        protected $eavSetupFactory;

        public function __construct(\Magento\Eav\Setup\EavSetupFactory $eavSetupFactory)
        {
            $this->eavSetupFactory = $eavSetupFactory;
        }

        public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
        {
            $setup->startSetup();
            $eavSetup = $this->eavSetupFactory->create();
            $entityTypeId = \Magento\Customer\Model\Customer::ENTITY; // value for eav_entity_type table, here 
            $eavSetup->updateAttribute($entityTypeId, 'email', 'is_required', false);
            $eavSetup->updateAttribute($entityTypeId, 'firstname', 'is_required', false);
            $eavSetup->updateAttribute($entityTypeId, 'lastname', 'is_required', false);
            $setup->endSetup();
        }
    }

}