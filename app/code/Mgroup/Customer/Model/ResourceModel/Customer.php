<?php
namespace Mgroup\Customer\Model\ResourceModel;

class Customer extends \Magento\Customer\Model\ResourceModel\Customer
{
    /**
     * Check customer scope, email and confirmation key before saving
     *
     * @param \Magento\Framework\DataObject $customer
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _beforeSave(\Magento\Framework\DataObject $customer)
    {
        /** @var \Magento\Customer\Model\Customer $customer */
        if ($customer->getStoreId() === null) {
            $customer->setStoreId($this->storeManager->getStore()->getId());
        }
        $customer->getGroupId();

        \Magento\Eav\Model\Entity\VersionControl\AbstractEntity::_beforeSave($customer);

        /*if (!$customer->getEmail()) {
            throw new ValidatorException(__('Please enter a customer email.'));
        }*/
        if ($customer->getEmail()) {
            $connection = $this->getConnection();
            $bind = ['email' => $customer->getEmail()];

            $select = $connection->select()->from(
                $this->getEntityTable(),
                [$this->getEntityIdField()]
            )->where(
                'email = :email'
            );
            if ($customer->getSharingConfig()->isWebsiteScope()) {
                $bind['website_id'] = (int)$customer->getWebsiteId();
                $select->where('website_id = :website_id');
            }
            if ($customer->getId()) {
                $bind['entity_id'] = (int)$customer->getId();
                $select->where('entity_id != :entity_id');
            }

            $result = $connection->fetchOne($select, $bind);
            if ($result) {
                throw new AlreadyExistsException(
                    __('A customer with the same email already exists in an associated website.')
                );
            }
        }
        

        // set confirmation key logic
        if ($customer->getForceConfirmed() || $customer->getPasswordHash() == '') {
            $customer->setConfirmation(null);
        } elseif (!$customer->getId() && $customer->isConfirmationRequired()) {
            $customer->setConfirmation($customer->getRandomConfirmationKey());
        }
        // remove customer confirmation key from database, if empty
        if (!$customer->getConfirmation()) {
            $customer->setConfirmation(null);
        }

        $this->_validate($customer);

        return $this;
    }
}
?>